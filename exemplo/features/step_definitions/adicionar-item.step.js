'use strict'

var GroceryList = require(process.cwd() + '/model/grocery-list');

var assert = require('assert');

module.exports = function() {

	var myList;
	var listItem = 'apple';

	this.Given(/^minha lista de compras vazia$/, function (callback) {
		myList = GroceryList.create();
		callback();
	});

	this.When(/^adiciono um item$/, function (callback) {
		myList.add(listItem);
		callback();
	});

	this.Then(/^a lista contem um item$/, function (callback) {
		assert.equal(myList.getAll().length, 1, 'Foi adicionado um elemento na lista.')
		callback();
	});

	this.Then(/^posso acessa-lo pela lista$/, function (callback) {
		assert.notEqual(myList.getItemIndex(listItem), -1, 'Item deve ser acessado a partir de indice positivo.');
		callback();
	});
}