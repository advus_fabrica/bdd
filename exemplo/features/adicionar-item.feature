Feature: Comprador pode adicionar item à lista de compras
	Como comprador quero adicionar item em minha lista, assim lembro de comprá-la no mercado.
		Scenario: Item adicionado à lista
			Given minha lista de compras vazia
			When adiciono um item 
			Then a lista contem um item
		Scenario: Item da lista acessível
			Given minha lista de compras vazia
			When adiciono um item
			Then posso acessa-lo pela lista